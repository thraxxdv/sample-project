import React from 'react'

function ListLoop(props) {
    const products = props.products
    return (
        <div>
            {
                products.map((obj) => <p>{obj}</p>)
            }            
        </div>
    )
}

export default ListLoop

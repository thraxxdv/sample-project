import React from 'react'
import ListLoop from './ListLoop';
import Prompts from './Prompts'

function ProductList(props) {

    const products = props.products;

    return (
        <div>
            { products.length == 0 ? <Prompts message="You do not have any products" /> : <ListLoop products={products} />}            
        </div>
    )
}

export default ProductList

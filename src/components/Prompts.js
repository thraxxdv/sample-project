import React from 'react'

function Prompts(props) {

    const msg = props.message;

    return (
        <div>
            <p>{msg}</p>
        </div>
    )
}

export default Prompts

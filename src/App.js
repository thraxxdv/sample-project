import './App.css';
import { useState } from 'react';
import ProductList from './components/ProductList';

function App() {

  const [products, setProducts] = useState([]);
  const [product, setProduct] = useState('');

  const handleSubmit = (e) => {
    e.preventDefault();
    products.push(product);
    setProduct('');
    console.log(products);
  }

  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={handleSubmit}>
          <input type="text" value={product} onChange={(e) => setProduct(e.target.value)} />
          <input type="submit" />
        </form>
        <ProductList products={products}/>
      </header>
    </div>
  );
}

export default App;
